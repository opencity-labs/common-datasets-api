# Common Datasets API

API che espone dataset di comune utilità per lo sviluppo di servizi digitali per la PA

I dataset contenuti:
- [/comuni](https://api.opencityitalia.it/datasets/comuni), fonte: [istat](https://www.istat.it/storage/codici-unita-amministrative/Elenco-comuni-italiani.csv)
- [/countries](https://api.opencityitalia.it/datasets/countries)
- [/pagopa](https://api.opencityitalia.it/datasets/pagopa), fonte: [PagoPA](https://raw.githubusercontent.com/pagopa/pagopa-api/develop/taxonomy/tassonomia.json)


# Local test

clone repo in your pc

```
$ docker-compose up -d
$ http http://localhost:80/
```

# Casi d'uso

* Anagrafica
* Payment Proxy che espongono la tassonomia di pagopa
* Widget Segnalazioni per realizzare il bounding-box
