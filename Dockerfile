FROM clue/json-server
WORKDIR /srv
COPY db.json .

CMD [ '--read-only', '--no-cors', '/srv/db.json' ]
