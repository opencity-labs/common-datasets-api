# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

image: docker:stable

stages:
  - preliminary-test
  - build
  - deploy
  - test

build-image:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag="latest"
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = $tag"
      elif [[ -n "$CI_COMMIT_TAG" ]]; then
        tag="$CI_COMMIT_TAG"
        echo "Running on tag '$CI_COMMIT_TAG': tag = $tag"
      else
        tag="$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE:${tag}" .
    - docker push "$CI_REGISTRY_IMAGE:${tag}"

check-db:
  image: lorello/alpine-bash:1.3.0
  stage: preliminary-test
  script: ./tests.sh


deploy_tag:
  stage: deploy
  when: manual
  environment:
    name: qa 
  tags:
    - boat-deploy
  script:
    - |
      if [[ -z $SWARM_SERVICE_QA ]]; then
        echo "Error, missing SWARM_SERVICE_QA variable, cannot deploy"
        exit 1
      fi
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag="latest"
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = $tag"
      elif [[ -n "$CI_COMMIT_TAG" ]]; then
        tag="$CI_COMMIT_TAG"
        echo "Running on tag '$CI_COMMIT_TAG': tag = $tag"
      else
        tag="$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
      docker service update --image "$CI_REGISTRY_IMAGE:${tag}" $SWARM_SERVICE_QA
  only:
    - tags
    - master

end2end_test_qa:
  stage: test
  image: 
    name: ghcr.io/orange-opensource/hurl:latest
    entrypoint: [ "" ]
  script:
    - hurl --test --variable base_url=https://api-qa.opencityitalia.it/datasets --glob "./tests/*.hurl"

end2end_test_prod:
  stage: test
  when: manual
  image: 
    name: ghcr.io/orange-opensource/hurl:latest
    entrypoint: [ "" ]
  script:
    - hurl --test --variable base_url=https://api.opencityitalia.it/datasets --glob "./tests/*.hurl"


prepare_job:
  stage: build
  image: alpine:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
  variables:
    TOKEN: $OC_RELEASE_MANAGER
  script:
    - apk add jq httpie
    - http --ignore-stdin GET "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/changelog?version=$CI_COMMIT_TAG" PRIVATE-TOKEN:$TOKEN | jq -r .notes > release_notes.md
    - cat release_notes.md
    - http --ignore-stdin POST "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/changelog" PRIVATE-TOKEN:$TOKEN version="$CI_COMMIT_TAG"
  artifacts:
    paths:
    - release_notes.md

release_job:
  stage: build
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_job
      artifacts: true
  rules:
  - if: '$CI_COMMIT_TAG =~ /^v?\d+\.\d+\.\d+$/'
  script:
    - echo "Creating release"
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: release_notes.md
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'Container Image $CI_COMMIT_TAG'
          url: "https://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
        - name: 'Full platform release notes'
          url: "https://docs.opencityitalia.it/v/operations/release/versione-2/$CI_COMMIT_TAG"


sast:
  stage: preliminary-test 


include:
- template: Security/SAST.gitlab-ci.yml




