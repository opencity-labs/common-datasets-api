#!/bin/bash

if [[ ! -f db.json ]]; then
	echo "Missing db.json"
	exit 1
fi

db_list=$(jq 'keys' db.json | jq -r '.[]')
db_number=$(echo $db_list | wc -w)
if [[ $db_number -ne 3 ]]; then
	echo "Unexpected number of databases, excpected 3, got $db_number"
	exit 2
fi

# check on pagopa
known_value="1201111TS"
if ! grep -q "$known_value" db.json; then
	echo "Failed check on db pagopa: missing value '$known_value'"
	exit 3
else
	echo "Successfully found known value '$known_value' in db pagopa"
fi

# check on comuni 
known_value="vicopisano"
result=$(jq -r ".comuni[] | select (.slug == \"$known_value\") | .slug" db.json)
if [[ $result != $known_value ]]; then
	echo "Failed check on db comuni: missing value '$known_value'"
	exit 3
else
	echo "Successfully found known value '$known_value' in db comuni"
fi

# check on countries
known_value="Italy"
result=$(jq -r ".countries[] | select (.countryName == \"$known_value\") | .countryName" db.json)
if [[ $result != $known_value ]]; then
	echo "Failed check on db countries: missing value '$known_value'"
	exit 3
else
	echo "Successfully found known value '$known_value' in db countries"
fi

